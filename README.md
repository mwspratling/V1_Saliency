----------------------------------------------------------------------------
# INTRODUCTION
----------------------------------------------------------------------------

This code implements the simulation results reported in:

[M. W. Spratling (2012) Predictive coding as a model of the V1 saliency map
hypothesis, Neural Networks, 26:7-28.](https://nms.kcl.ac.uk/michael.spratling/Doc/v1_saliency.pdf)

Please cite this paper if this code is used in, or to motivate, any publications. 

----------------------------------------------------------------------------
# USAGE
----------------------------------------------------------------------------

This code requires MATLAB and the Image Processing Toolbox. It was tested with
MATLAB Version 7.7 (R2008b) and Image Processing Toolbox Version 6.2 (R2008b).

To use this software:
```
    run matlab 
    cd to the directory containing this code 
    run one of the functions with the name V1_*
```

Each of the V1_* functions runs one of the experiments reported in the above
publication. The following table gives details of which MATLAB function produces
each figure in this article.

Simulations will run much faster if you install the MATLAB function convnfft 
written by Bruno Luong, which is available here:
http://www.mathworks.com/matlabcentral/fileexchange/24504-fft-based-convolution

Figure    |  MATLAB Function
----------|-------------------------------------------------------------
3a        |  V1_bar_and_texture_errors
3b        |  V1_texture_border_errors
4a        |  V1_border_orientation
4b        |  V1_border_orientation (uncomment the line beginning: angle_jitter=15)
5a        |  V1_border_spacing
5b        |  V1_border_length
6         |  V1_border_spacing_luminance
7         |  V1_border_zhaoping_may
          |
8         |  V1_feature_efficiency
9a        |  V1_feature_category
9b        |  V1_feature_symmetry
10        |  V1_feature_efficiency_circles
11        |  V1_feature_efficiency_luminance
12a       |  V1_feature_asymmetric1
12b       |  V1_feature_asymmetric2
13        |  V1_asymmetric_feature_errors
14        |  V1_feature_spacing
15        |  V1_feature_zhaoping_may
16        |  V1_feature_onset
17        |  V1_feature_preview
          |
18        |  V1_change_blindness
          |
19b       |  V1_border_attention
20        |  V1_contour
21        |  V1_feature_attention

