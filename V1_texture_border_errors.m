function V1_texture_border_errors
[imsizefac,crop,max_radius]=common_param_values;

%DEFINE TEST IMAGES
[barlen,barwid,spacing,gridsize]=standard_image_param_values(imsizefac,crop,1);
[I{1},ycoord{1},xcoord{1}]=image_border(gridsize,barlen,barwid,spacing,[90,0],[0.5,0]);
I{1}=I{1}';
[I{2},ycoord{2},xcoord{2}]=image_popout(gridsize,barlen,barwid,spacing,[0,0],[0,0.5]);
[I{3},ycoord{3},xcoord{3}]=image_border(gridsize,barlen,barwid,spacing,[90,20],[0.5,0.5]);
I{3}=I{3}';
[I{4},ycoord{4},xcoord{4}]=image_popout(gridsize,barlen,barwid,spacing,[70,0],[0.5,0.5]);
[I{5},ycoord{5},xcoord{5}]=image_border(gridsize,barlen,barwid,spacing,[90,0],[0.5,0.5]);
I{5}=I{5}';
[I{6},ycoord{6},xcoord{6}]=image_popout(gridsize,barlen,barwid,spacing,[90,0],[0.5,0.5]);
for test=1:length(I)
 	cent_radius{test}=min(ceil(0.5*barlen+spacing),max_radius);
end

%PERFORM EXPERIMENT AND PLOT RESULTS
[saliency_index,poo,e]=...
	perform_V1_saliency_experiment(I,cent_radius,ycoord,xcoord);
plot_error_histogram(I,e,1);
