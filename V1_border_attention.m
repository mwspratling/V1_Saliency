function V1_border_attention
[imsizefac,crop,max_radius]=common_param_values;

%DEFINE TEST IMAGES
[barlen,barwid,spacing,gridsize]=standard_image_param_values(imsizefac,crop,1);
gridsize=gridsize-1;

[I{1},ycoord{1}]=image_border(gridsize,barlen,barwid,spacing,[-45,45]);
[a,b]=size(I{1});
A{1}=initialise_feedback(I{1},[1:32],[ceil(a/2),a,1,b]);
[I{2},ycoord{2}]=image_border_overlayed(gridsize,barlen,barwid,spacing,[-45,45,0,90]);
[a,b]=size(I{2});
A{2}=initialise_feedback(I{2},[1:32],[ceil(a/2),a,1,b]);
%[I{1},ycoord{1}]=image_border(gridsize,barlen,barwid,spacing,[90,0]);
for test=1:length(I)
  cent_radius{test}=min(ceil(spacing),max_radius);
end
%PERFORM EXPERIMENT AND PLOT RESULTS
[saliency_index,poo,e]=...
	perform_V1_saliency_experiment(I,cent_radius,ycoord,[],[],A);
plot_error_histogram(I,e,1,saliency_index);

