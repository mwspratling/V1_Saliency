function [A]=initialise_feedback(I,masks,range)
% I = input image to PC/BC processing stage
% masks = a vector defining which masks (i.e. feature preferences) are to receive feedback
% range = a 4 element vector defining a rectangular spatial region that is to receive 
%         feedback

%find out how many prediction nodes there are
[imsizefac,crop,max_radius,sigma]=common_param_values;
for s=sigma
  w=filter_definitions_V1_simple(s);
end
[nMasks,nChannels]=size(w);
[a,b,z]=size(I);

%allow spatial range to be defined in absolute coordinate (if given as +ve values) or relative to the centre of the image (if given as -ve values)
if range(1)<0, range(1)=ceil(a/2)+range(1); end
if range(2)<0, range(2)=ceil(a/2)-range(2); end
if range(3)<0, range(3)=ceil(b/2)+range(3); end
if range(4)<0, range(4)=ceil(b/2)-range(4); end

%define feedback as zero everywhere except for the features/spatial locations specified
for i=1:nMasks
    A{i}=zeros(a,b,'single');
    if sum(i==masks)>0, 
      A{i}(range(1):range(2),range(3):range(4))=0.25; end
    end 
end
    
    
  %SPATIAL ATTENTION
  %spatial_spotlight=gauss2D(12,0,1,0,a,0,0);%attend centre
  %spatial_spotlight=gauss2D(3,0,1,0,a,fix(a/4),fix(a/4));%attend peripheral location
  %A{i}=spatial_spotlight.*0.75;
 
  %FEATURAL ATTENTION
  %if i>=17 & i<=24, A{i}(:)=1; end %attend one phase

