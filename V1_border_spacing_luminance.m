function V1_border_spacing_luminance
[imsizefac,crop,max_radius]=common_param_values;

%DEFINE TEST IMAGES
barwid=(13/60*imsizefac)
barlen=barwid;
pos_jitter=0;
angle_jitter=0;
trials=1;

%CREATE IMAGES
spacings=odd(barlen:2:160./60.*imsizefac)
for trial=1:trials
  for test=1:length(spacings)
	gridsize=5+odd(2*crop/spacings(test));
	%pos_jitter=0.125*spacings(test)
	[I{test},ycoord{test}]=image_border(gridsize,barlen,barwid,spacings(test),[0,0],[0.1,0.5],0.5,pos_jitter,angle_jitter);
	cent_radius{test}=min(ceil(spacings(test)),max_radius);
  end
  %PERFORM EXPERIMENT AND PLOT RESULTS
  saliency_index=perform_V1_saliency_experiment(I,cent_radius,ycoord);
end
if trials>1
  saliency_index=mean(saliency_index')';
end
%plot_saliency_histogram(I,saliency_index,[]);


figure(1),clf
%subplot(3,1,[2:3]);
axes('Position',[0.1300    0.2100    0.7750    0.5154]);
plot(spacings./imsizefac,saliency_index,'b-s','LineWidth',4,'MarkerSize',12,'MarkerFaceColor','w');
set(gca,'FontSize',14,'Box','off');
ylabel('Saliency Index');
xlabel('Spacing (degrees)');
set(gca,'YTick',[0:0.1:0.45],'YTickLabel',[0:0.1:0.45]);
axis([0,3,-0.05,0.475])

hsize=25;
plot_values=[1,3,5,8];
for test=1:length(plot_values)
  [a,b]=size(I{plot_values(test)});  
  axes('Position',[0.15+(test-1)*0.2,0.65,0.15,0.4]),
  imagesc(I{plot_values(test)}(ceil(a/2)-hsize:ceil(a/2)+hsize,ceil(b/2)-hsize:ceil(b/2)+hsize)',[0,1]);
  axis('equal','tight')
  set(gca,'XTick',[],'YTick',[]);
end
colormap('gray')
set(gcf,'PaperUnits','inches');
set(gcf,'PaperPosition',[0.95 0.418 6.36 3.33]);
