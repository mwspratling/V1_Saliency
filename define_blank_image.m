function [I,gridsize,xlocations,ylocations]=define_blank_image(gridsize,len,spacing,backgnd)
gridsize=(gridsize-1)*spacing+2+odd(len);
I=zeros(gridsize)+backgnd;

hlen=ceil(odd(len)/2);
%create regular texture
spacing=round(spacing);
xlocations=1+hlen:spacing:gridsize-hlen;
ylocations=1+hlen:spacing:gridsize-hlen;