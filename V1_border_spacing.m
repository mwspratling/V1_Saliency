function V1_border_spacing
[imsizefac,crop,max_radius]=common_param_values;

%DEFINE TEST IMAGES
barwid=(0.15*imsizefac)
angle=45;
pos_jitter=0;
angle_jitter=0;
trials=1;

%CREATE IMAGES
barlen=((39/60)*imsizefac)
spacings=odd((1/sqrt(2))*barlen:2:160./60.*imsizefac)
for trial=1:trials
  for test=1:length(spacings)
	gridsize=5+odd(2*crop/spacings(test));
	%pos_jitter=0.125*spacings(test)
	[I{test},ycoord{test}]=image_border(gridsize,barlen,barwid,spacings(test),[angle,-angle],[0.5,0.5],0.5,pos_jitter,angle_jitter);
	cent_radius{test}=min(ceil(spacings(test)),max_radius);
  end
  %PERFORM EXPERIMENT AND PLOT RESULTS
  saliency_index=perform_V1_saliency_experiment(I,cent_radius,ycoord);
end
if trials>1
  saliency_index=mean(saliency_index')';
end
figure(2),clf
plot_data('b-s',1,saliency_index,I,spacings,crop);
figure(1)


clear saliency_index I ycoord cent_radius
%CREATE IMAGES
barlen=((78/60)*imsizefac)
spacings=odd((1/sqrt(2))*barlen:2:160./60.*imsizefac)
for trial=1:trials
  for test=1:length(spacings)
	gridsize=5+odd(2*crop/spacings(test));
	%pos_jitter=0.125*spacings(test)
	[I{test},ycoord{test}]=image_border(gridsize,barlen,barwid,spacings(test),[angle,-angle],[0.5,0.5],0.5,pos_jitter,angle_jitter);
	cent_radius{test}=min(ceil(spacings(test)),max_radius);
  end
  %PERFORM EXPERIMENT AND PLOT RESULTS
  saliency_index=perform_V1_saliency_experiment(I,cent_radius,ycoord);
end
if trials>1
  saliency_index=mean(saliency_index')';
end
figure(2)
plot_data('r-o',2,saliency_index,I,spacings,crop);
figure(1)




function plot_data(line_style,number,saliency,I,vars,crop)
[imsizefac,crop,max_radius]=common_param_values;
subplot(2,2,3)
if number==1, plot([0,5],[0,0],'k-'), hold on, end
plot(vars./imsizefac,saliency,line_style,'LineWidth',3,'MarkerSize',10,'MarkerFaceColor','w');
hold on
axis([0,3,-0.1,0.4])
set(gca,'FontSize',12,'Box','off');
ylabel('Saliency Index');
xlabel('Bar Spacing (degrees)');


maxsubplot(4,8,5+(number-1)*8),
plot(0,0,line_style,'LineWidth',3,'MarkerSize',10,'MarkerFaceColor','w');
axis([-0.5,1,-1,1])
axis off;

hsize=25;
nvars=length(I);
plot_vars=[1,ceil(nvars/2),nvars]
for test=1:length(plot_vars)
  [a,b]=size(I{plot_vars(test)});  
  maxsubplot(4,7,4+test+(number-1)*7),
  imagesc(I{plot_vars(test)}(ceil(a/2)-hsize:ceil(a/2)+hsize,ceil(b/2)-hsize:ceil(b/2)+hsize)',[0,1]);
  axis('equal','tight')
  set(gca,'XTick',[],'YTick',[]);
end
colormap('gray')
set(gcf,'PaperUnits','inches');
set(gcf,'PaperPosition',[0.95 0.4 6 3.5]);
