function [saliency_index_end,saliency_index,e_max_valid]=perform_V1_saliency_experiment(I,cent_radius,ycoord,xcoord,clen,A)
[imsizefac,crop,max_radius,sigma]=common_param_values;
iterations=10;
ntrials=length(I);
if ~exist('xcoord','var') || isempty(xcoord), for i=1:ntrials, xcoord{i}=[]; end, end
if ~exist('clen','var') || isempty(clen), for i=1:ntrials, clen{i}=[]; end, end
if ~exist('A','var') || isempty(A), for i=1:ntrials, A{i}=[]; end, end


%define v1 prediction node receptive fields
w=[];
for s=sigma
  w=filter_definitions_V1_simple(s,w);
end

cmap=colormap('gray'); cmap=1-cmap; colormap(cmap);
for trial=1:ntrials
  %figure(trial+1); 
  clf
  %plot input image
  [a,b,z]=size(I{trial});  
  maxsubplot(1,4,1),imagesc(I{trial}(crop+1:a-crop,crop+1:b-crop,z));
  axis('equal','tight'),colorbar('SouthOutside'); title('I')
  
  %LGN preprocessing of image
  [X]=preprocess_V1_input(I{trial},1.5);
  maxsubplot(2,4,2),imagesc(X{1}(crop+1:a-crop,crop+1:b-crop,z));
  axis('equal','tight'), title('Xon')
  maxsubplot(2,4,6),imagesc(X{2}(crop+1:a-crop,crop+1:b-crop,z));
  axis('equal','tight'), title('Xoff')
  
  %Simulate the \PCBC model
  [y,r,e,ytrace,rtrace,etrace]=network_dim_conv(w,X,iterations,A{trial});

  %plot response of prediction nodes (taking max across filter types)
  maxsubplot(2,4,3),
  ytmp=max(cat(3,y{:}),[],3);
  ytmp=ytmp(crop+1:a-crop,crop+1:b-crop);
  imagesc(ytmp);
  axis('equal','tight'), title('Y'), ylabel('Y');
  %yplot(1,1:iterations)=ytrace{3}(xcoord{trial},ycoord{trial},:);
  %yplot(2,1:iterations)=ytrace{7}(xcoord{trial}+20,ycoord{trial}+20,:);
  %yplot(3,1:iterations)=ytrace{7}(xcoord{trial},ycoord{trial},:);
  %maxsubplot(2,4,3),plot(yplot')
  
  %plot response of error nodes (taking max across input channels)
  maxsubplot(2,4,7),
  etmp=max(cat(3,e{:}),[],3);
  etmp=etmp(crop+1:a-crop,crop+1:b-crop);
  imagesc(etmp);
  axis('equal','tight'), title('E'), ylabel('E');
  saliency_index(trial,:)= ...
	  analyse_saliency(max(cat(4,etrace{:}),[],4),crop,ycoord{trial},cent_radius{trial},xcoord{trial},clen{trial}); 
  e_max_valid{trial}=etmp;
  drawnow;
  %figure(3), ecrosssec=max(etmp'); plot(ecrosssec), hold on, figure(1);
end
saliency_index_end=saliency_index(:,iterations);
