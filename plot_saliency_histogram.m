function plot_saliency_histogram(I,saliency,expected_saliency,scale,iconstep)
ntrials=length(I);
if nargin>2 && ~isempty(expected_saliency)
  saliency=[saliency,scale./expected_saliency];
  bwidth=0.8;
else
  bwidth=0.33;
end
if nargin<5 || isempty(iconstep)
  iconstep=1; 
end
trialspacing=0.09;

figure(1),clf
axes('Position',[0.12,0.55,0.09+trialspacing*ntrials,0.4]),
if ntrials>1
  bar([1:ntrials],saliency,bwidth);
else
  bar(saliency)
end
maxSI=max(0,max(max(saliency)));
axis([0,ntrials+1,-0.005,ceil(maxSI*100)./100])
colormap('gray')
set(gca,'XTickLabel',[],'FontSize',8,'Box','off');
ylabel('Saliency Index');
if nargin>3 && ~isempty(scale)
  ax2 = axes('Position',get(gca,'Position'),'YAxisLocation','right','FontSize',8,'Color','none','XColor','k','YColor','k');
  axis([0,ntrials+1,-0.005,ceil(maxSI*100)./100])
  %ylabel('Normalised Reaction Time')
  ylabel('Reaction Time (ms)')
  points=[0.025,0.05,0.1,0.2];
  set(ax2,'YTick',points,'YTickLabel',num2str(scale./points'),'XTickLabel',[]);
end
plot_histogram_icons(I,iconstep)
