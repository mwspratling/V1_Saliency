function [I,xcent,ycent]=image_popout_vernier(gridsize,len,wid,spacing,angle,contrast,backgnd,jitter_pos,jitter_ang,surr_prob,cent_vernier)
%draw a uniform texture with a single element at a different orientation
%and/or contrast
if nargin<6, contrast=0.5*ones(size(angle)); end
if nargin<7, backgnd=0.5; end
if nargin<8, jitter_pos=0; end
if nargin<9, jitter_ang=0; end
if nargin<10, surr_prob=1; end
if nargin<11, cent_vernier=1; end

[I,gridsize,xlocations,ylocations]=define_blank_image(gridsize,len,spacing,backgnd);

vernier_offset=1;
bar=define_bar(0.5*len,wid);
blank=define_bar(len,wid).*0;
[as,bs]=size(bar);
[a,b]=size(blank);
yval1=1+fix((bs-1)/2);
yval2=b-fix((bs-1)/2);
if yval2-yval1>ceil(bs/2); yval1=yval1+1; yval2=yval2-1; end
vernier=draw_bar(blank,ceil(a/2)+vernier_offset,yval1,bar,0,1,0);
vernier=draw_bar(vernier,ceil(a/2),yval2,bar,0,1,0);

bar=define_bar(len,wid);

a=0;
for x=xlocations
  a=a+1;
  b=0;
  for y=ylocations
	b=b+1;
	xval=fix(x+(2*jitter_pos*rand)-jitter_pos);
	yval=fix(y+(2*jitter_pos*rand)-jitter_pos);
	angleval=angle+(2*jitter_ang*rand)-jitter_ang;
	if a==ceil(length(xlocations)/2) & b==ceil(length(ylocations)/2)
	  if cent_vernier
		  I=draw_bar(I,xval,yval,bar,angleval(1),contrast(1),backgnd);
		else
		  I=draw_bar(I,xval,yval,vernier,angleval(1),contrast(1),backgnd);
	  end
	  xcent=x;
	  ycent=y;
	else
	  if rand<=surr_prob
		if cent_vernier		
		  I=draw_bar(I,xval,yval,vernier,angleval(2),contrast(2),backgnd);
		else
		  I=draw_bar(I,xval,yval,bar,angleval(2),contrast(2),backgnd);
		end
	  end
	end
  end
end
