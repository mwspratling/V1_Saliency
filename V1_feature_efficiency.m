function V1_feature_efficiency
[imsizefac,crop,max_radius]=common_param_values;

%DEFINE TEST IMAGES
[barlen,barwid,spacing,gridsize]=standard_image_param_values(imsizefac,crop,1);
trials=20;

target_ang=45;
for trial=1:trials
  trial
  %CREATE IMAGES
  [I{1},ycoord{1},xcoord{1}]=image_popout(gridsize,barlen,barwid,spacing,[target_ang,target_ang-90]);
  [I{2},ycoord{2},xcoord{2}]=image_popout(gridsize,barlen,barwid,spacing,[target_ang,target_ang-15]);
  [I{3},ycoord{3},xcoord{3}]=image_popout(gridsize,barlen,barwid,spacing,[target_ang,target_ang-30],[0.5,0.5],0.5,0,15);
  [I{4},ycoord{4},xcoord{4}]=image_popout(gridsize,barlen,barwid,spacing,[target_ang,target_ang-30],[0.5,0.5],0.5,0,0,0.5);
  [I{5},ycoord{5},xcoord{5}]=image_popout_dualdist(gridsize,barlen,barwid,spacing,[target_ang,target_ang-20,target_ang+20]);
  [I{6},ycoord{6},xcoord{6}]=image_feature_conj(gridsize,barlen,barwid,spacing,[45,0,90,-45]);

  for test=1:length(I)
	cent_radius{test}=min(ceil(0.5*barlen+spacing),max_radius);
  end
  
  %PERFORM EXPERIMENT AND PLOT RESULTS
  saliency_index(:,trial)=...
	  perform_V1_saliency_experiment(I,cent_radius,ycoord,xcoord);
end
if trials>1
  saliency_index=mean(saliency_index')';
end
plot_saliency_histogram(I,saliency_index);

