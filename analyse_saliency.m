function [saliency_index]=analyse_saliency(R,crop,ycoord,radius,xcoord,clength)
[a,b,iterations]=size(R);
Resp=R(crop+1:a-crop,crop+1:b-crop,:);
[a,b,iterations]=size(Resp);
[mresp,bb]=max(Resp(:,:,iterations)); [mmresp,aa]=max(mresp); bb=bb(aa);
disp(['overall max at (',int2str(aa),',',int2str(bb),')=',num2str(mmresp)]);

if isempty(xcoord) && isempty(clength) %texture border 
  %decide location of border
  ycoord=fix(ycoord-crop);

  %split response into two components: near border and away from border
  Rcent=Resp(ycoord-radius:ycoord+radius,:,:);
  Rperif=Resp;
  Rperif(ycoord-radius:ycoord+radius,:,:)=0;
elseif isempty(clength) %feature search
  %decide location of border
  ycoord=fix(ycoord-crop);
  xcoord=fix(xcoord-crop);

  %split response into two components: near feature and away from feature
  Rcent=Resp(ycoord-radius:ycoord+radius,xcoord-radius:xcoord+radius,:);
  Rperif=Resp;
  Rperif(ycoord-radius:ycoord+radius,xcoord-radius:xcoord+radius,:)=0;
else %contour stimulus 
  %decide location of contour
  ycoord=fix(ycoord-crop);
  xcoord=fix(xcoord-crop);

  %split response into two components: near contour and away from contour
  Rcent=Resp(ycoord-radius:ycoord+radius,max(1,xcoord-clength):min(b,xcoord+clength),:);
  Rperif=Resp;
  Rperif(ycoord-radius:ycoord+radius,max(1,xcoord-clength):min(b,xcoord+clength),:)=0;
end


for t=1:iterations
  max_cent=max(max(Rcent(:,:,t)));
  max_perif=max(max(Rperif(:,:,t)));
  saliency_index(t)=(max_cent-max_perif)/(max_cent+max_perif);
  %saliency_count(t)=-0.01*sum(sum(Rperif(:,:,t)>=max_cent));
  %requires vl_feat toolbox, run vl_setup before this code!
  %rperiftmp=double(Rperif(:,:,t));
  %localmaxindeces=vl_localmax(rperiftmp);
  %rperiftmp=rperiftmp(:);
  %saliency_count(t)=-0.01*sum(rperiftmp(localmaxindeces)>=max_cent);
end
saliency_index=sign(saliency_index).*sqrt(abs(saliency_index));

power=5;
scalemax=max(1.000001,max(max(Resp(:,:,iterations).^power)));
maxsubplot(2,4,8),imagesc(Rperif(:,:,iterations).^power,[2,scalemax]), axis('equal','tight'),set(gca,'XTick',[],'YTick',[]);
maxsubplot(2,4,4),imagesc(Rcent(:,:,iterations).^power,[2,scalemax]), axis('equal','tight'),set(gca,'XTick',[],'YTick',[]);
title(['SI=',num2str(saliency_index(iterations))]);