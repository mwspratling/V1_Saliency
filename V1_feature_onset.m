function V1_feature_onset
[imsizefac,crop,max_radius]=common_param_values;
onset_time=6;

%DEFINE TEST IMAGES
[barlen,barwid,spacing,gridsize]=standard_image_param_values(imsizefac,crop,1);

test=0;

[final_image,yval,xval]=image_popout(gridsize,barlen,barwid,spacing,[45,-45],[0.5,0.5]);
mask_image=image_feature_conj(gridsize,barlen,barwid,spacing,[45,-45,-45,45],[0,0.5]);

%CREATE IMAGES
%1) onset target, non-onset distractors
test=test+1;
for t=1:onset_time-1
  I{test}(:,:,t)=mask_image;
end
for t=onset_time:10
  I{test}(:,:,t)=final_image;
end
ycoord{test}=yval;
xcoord{test}=xval;

%2) onset target, onset distractors - distractors onset 1st
test=test+1;
for t=1:onset_time-1
  I{test}(:,:,t)=image_popout(gridsize,barlen,barwid,spacing,[45,-45],[0,0.5]);
end
for t=onset_time:10
  I{test}(:,:,t)=final_image;
end
ycoord{test}=yval;
xcoord{test}=xval;

%3) onset target, onset distractors
test=test+1;
for t=1:onset_time-1
  I{test}(:,:,t)=0.5+0.*mask_image;
end
for t=onset_time:10
  I{test}(:,:,t)=final_image;
end
ycoord{test}=yval;
xcoord{test}=xval;


for test=1:length(I)
  cent_radius{test}=min(ceil(0.5*barlen+spacing),max_radius);
end

%PERFORM EXPERIMENT AND PLOT RESULTS
saliency_index=perform_V1_saliency_experiment(I,cent_radius,ycoord,xcoord);
%pad saliency and images to allow more spacing in plot of results
saliency_index(5)=saliency_index(3);
I{5}=I{3};
saliency_index(3)=saliency_index(2);
I{3}=I{2};
saliency_index(2)=NaN;
saliency_index(4)=NaN;
plot_saliency_histogram(I,saliency_index,[],[],2);

