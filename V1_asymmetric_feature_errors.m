function V1_feature_errors
[imsizefac,crop,max_radius]=common_param_values;

if 0
[I{1},ycoord{1},xcoord{1}]=image_feature_add(gridsize,barlen,barwid,spacing,0,[0.5,0]);%cross
for test=1:length(I)
 	cent_radius{test}=min(ceil(0.5*barlen+spacing),max_radius);
end
%PERFORM EXPERIMENT AND PLOT RESULTS
[saliency_index,poo,e]=...
	perform_V1_saliency_experiment(I,cent_radius,ycoord,xcoord);
plot_error_histogram(I,e,1);
end


%DEFINE TEST IMAGES
[barlen,barwid,spacing,gridsize]=standard_image_param_values(imsizefac,crop,2);
[I{1},ycoord{1},xcoord{1}]=image_popout_ellipses(gridsize,barlen,barwid,spacing,[0,0],[0.5,0],0.5,0,0,1,0);%circle
[I{2},ycoord{2},xcoord{2}]=image_popout_ellipses(gridsize,barlen,barwid,spacing,[0,0],[0.5,0],0.5,0,0,1,1);%elipse
[I{3},ycoord{3},xcoord{3}]=image_popout_curves(gridsize,barlen,barwid,spacing,[0,0],[0.5,0],0.5,0,0,1,0,barlen*2);%curve
[I{4},ycoord{4},xcoord{4}]=image_popout_circles(gridsize,barlen,barwid,spacing,[-90,-90],[0.5,0],0.5,0,0,1,1);%circle with gap
[I{5},ycoord{5},xcoord{5}]=image_popout_circle_bars(gridsize,barlen,barwid,spacing,[0,0],[0.5,0],0.5,0,0,1,0);%small circle
[I{6},ycoord{6},xcoord{6}]=image_popout_circle_bars(gridsize,barlen,barwid,spacing,[0,0],[0.5,0],0.5,0,0,1,1);%circle with bar

for test=1:length(I)
 	cent_radius{test}=min(ceil(0.5*barlen+spacing),max_radius);
end

%PERFORM EXPERIMENT AND PLOT RESULTS
[saliency_index,poo,e]=...
	perform_V1_saliency_experiment(I,cent_radius,ycoord,xcoord);
plot_error_histogram(I,e,1);