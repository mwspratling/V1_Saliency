function V1_feature_zhaoping_may
[imsizefac,crop,max_radius]=common_param_values;

%DEFINE TEST IMAGES
[barlen,barwid,spacing,gridsize]=standard_image_param_values(imsizefac,crop,1);
[I{1},ycoord{1},xcoord{1}]=image_popout(gridsize,barlen,barwid,spacing,[-45,45]);
[I{5},ycoord{5},xcoord{5}]=image_popout(gridsize,barlen,barwid,spacing,[90,0]);
[I{4},ycoord{4},xcoord{4}]=image_popout_overlayed(gridsize,barlen,barwid,spacing,[-45,45,0,90]);
[I{7},ycoord{7},xcoord{7}]=image_popout_overlayed(gridsize,barlen,barwid,spacing,[90,0,-45,45]);
[I{2},ycoord{2},xcoord{2}]=image_popout_overlayed(gridsize,barlen,barwid,spacing,[-45,45,90,90]);
[I{6},ycoord{6},xcoord{6}]=image_popout_overlayed(gridsize,barlen,barwid,spacing,[90,0,-45,-45]);
[I{3},ycoord{3},xcoord{3}]=image_popout_overlayed(gridsize,barlen,barwid,spacing,[-45,45,0,0]);
for test=1:length(I)
  cent_radius{test}=min(ceil(0.5*barlen+spacing),max_radius);
end
%PERFORM EXPERIMENT AND PLOT RESULTS
saliency_index=perform_V1_saliency_experiment(I,cent_radius,ycoord,xcoord);

%Zhoaping and May results (exact grand average over 5 subjects), RTs (ms):
RT=[462.6 607.4  646.8 2073.8 451.4 656.2 1069.8];
plot_saliency_histogram(I,saliency_index,RT',100);
%plot_saliency_histogram(I,saliency_index,[]);


