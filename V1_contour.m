function V1_contour
[imsizefac,crop,max_radius]=common_param_values;

%DEFINE TEST IMAGES
[barlen,barwid,spacing,gridsize]=standard_image_param_values(imsizefac,crop,1);
gridsize=gridsize+2;

%spacing=spacing-1;
trials=20;

for trial=1:trials
  trial
  %CREATE IMAGES
  test=0;
  for clen=1:3
    test=test+1;
    [I{test},ycoord{test},xcoord{test}]=image_contour(gridsize,barlen,barwid,spacing,0,clen);
    clength{test}=ceil(0.5*barlen+(spacing*(clen+1)));
    cent_radius{test}=min(ceil(0.5*barlen+spacing),max_radius);
    A{test}=initialise_feedback(I{test},[1:8:32],[-4,-4,-(clength{test}-spacing),-(clength{test}-spacing)]);
  end
  %PERFORM EXPERIMENT AND PLOT RESULTS
  saliency_index(:,trial)=...
    perform_V1_saliency_experiment(I,cent_radius,ycoord,xcoord,clength,A);
end
if trials>1
  std(saliency_index')'
  saliency_index=mean(saliency_index')';
end
plot_saliency_histogram(I,saliency_index);
