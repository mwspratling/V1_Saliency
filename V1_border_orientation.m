function V1_border_orientation
[imsizefac,crop,max_radius]=common_param_values;

%DEFINE TEST IMAGES
[barlen,barwid,spacing,gridsize]=standard_image_param_values(imsizefac,crop,1);
gridsize=gridsize-1;
angles=[0:22.5:90];
angle_jitter=0;
pos_jitter=0;
trials=1;
%angle_jitter=15; trials=20; %if angle_jitter>0 it is necessary to carry out multple trials
saliency_sem=[];

%CREATE IMAGES
for trial=1:trials
  test=0;
  for angle=angles
    test=test+1;
    [I{test},ycoord{test}]=image_border(gridsize,barlen,barwid,spacing,angle/2,[0.5,0.5],0.5,pos_jitter,angle_jitter);
    cent_radius{test}=min(ceil(spacing),max_radius);
  end
  %PERFORM EXPERIMENT AND PLOT RESULTS
  saliency_index(:,trial)=perform_V1_saliency_experiment(I,cent_radius,ycoord);
end
if trials>1
  saliency_sem=std(saliency_index')'./sqrt(trials);
  saliency_index=mean(saliency_index')';
end
figure(2),clf
plot_data('b-s',1,saliency_index,I,angles,crop,saliency_sem);
figure(1)


%CREATE IMAGES
for trial=1:trials
  test=0;
  for angle=angles
    test=test+1;
    [I{test},ycoord{test}]=image_border(gridsize,barlen,barwid,spacing,90-angle/2,[0.5,0.5],0.5,pos_jitter,angle_jitter);
  end
  %PERFORM EXPERIMENT AND PLOT RESULTS
  saliency_index(:,trial)=perform_V1_saliency_experiment(I,cent_radius,ycoord);
end
if trials>1
  saliency_sem=std(saliency_index')'./sqrt(trials);
  saliency_index=mean(saliency_index')';
end
figure(2)
plot_data('r-o',2,saliency_index,I,angles,crop,saliency_sem);
figure(1)


%CREATE IMAGES
for trial=1:trials
  test=0;
  for angle=angles
    test=test+1;
    [I{test},ycoord{test}]=image_border(gridsize,barlen,barwid,spacing,[angle,0],[0.5,0.5],0.5,pos_jitter,angle_jitter);
  end
  %PERFORM EXPERIMENT AND PLOT RESULTS
  saliency_index(:,trial)=perform_V1_saliency_experiment(I,cent_radius,ycoord);
end
if trials>1
  saliency_sem=std(saliency_index')'./sqrt(trials);
  saliency_index=mean(saliency_index')';
end
figure(2)
plot_data('g-d',3,saliency_index,I,angles,crop,saliency_sem);
figure(1)


%CREATE IMAGES
for trial=1:trials
  test=0;
  for angle=angles
    test=test+1;
    [I{test},ycoord{test}]=image_border(gridsize,barlen,barwid,spacing,[90-angle,90],[0.5,0.5],0.5,pos_jitter,angle_jitter);
  end
  %PERFORM EXPERIMENT AND PLOT RESULTS
  saliency_index(:,trial)=perform_V1_saliency_experiment(I,cent_radius,ycoord);
end
if trials>1
  saliency_sem=std(saliency_index')'./sqrt(trials);
  saliency_index=mean(saliency_index')';
end
figure(2)
plot_data('c-v',4,saliency_index,I,angles,crop,saliency_sem);
figure(1)





function plot_data(line_style,number,saliency,I,angles,crop,SEM)
subplot(1,2,1)
if number==1, plot([0,100],[0,0],'k-'), hold on, end
if ~isempty(SEM), errorbar(angles,saliency,SEM,line_style(1)); hold on, end
plot(angles,saliency,line_style,'LineWidth',3,'MarkerSize',10,'MarkerFaceColor','w');
hold on
axis([-1,100,-0.1,0.3])
set(gca,'XTick',[0:45:90],'XTickLabel',[0:45:90],'FontSize',12,'Box','off');
ylabel('Saliency Index');
xlabel('Orientation Contrast');


maxsubplot(4,8,5+(number-1)*8),
plot(0,0,line_style,'LineWidth',3,'MarkerSize',10,'MarkerFaceColor','w');
axis([-0.5,1,-1,1])
axis off;

hsize=25;
nangles=length(I);
plot_angles=[1,ceil(nangles/2),nangles];
for test=1:length(plot_angles)
  [a,b]=size(I{plot_angles(test)});
  maxsubplot(4,7,4+test+(number-1)*7),
  imagesc(I{plot_angles(test)}(ceil(a/2)-hsize:ceil(a/2)+hsize,ceil(b/2)-hsize:ceil(b/2)+hsize)',[0,1]);
  axis('equal','tight')
  set(gca,'XTick',[],'YTick',[]);
end
colormap('gray')
set(gcf,'PaperUnits','inches');
set(gcf,'PaperPosition',[0.95 0.4 6 3.5]);
