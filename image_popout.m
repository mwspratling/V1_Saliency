function [I,xcent,ycent]=image_popout(gridsize,len,wid,spacing,angle,contrast,backgnd,jitter_pos,jitter_ang,surr_prob)
%draw a uniform texture with a single element at a different orientation
%and/or contrast
if nargin<6, contrast=0.5*ones(size(angle)); end
if nargin<7, backgnd=0.5; end
if nargin<8, jitter_pos=0; end
if nargin<9, jitter_ang=0; end
if nargin<10, surr_prob=1; end

bar=define_bar(len,wid);
[I,gridsize,xlocations,ylocations]=define_blank_image(gridsize,len,spacing,backgnd);

a=0;
for x=xlocations
  a=a+1;
  b=0;
  for y=ylocations
    b=b+1;
    xval=fix(x+(2*jitter_pos*rand)-jitter_pos);
    yval=fix(y+(2*jitter_pos*rand)-jitter_pos);
    angleval=angle+(2*jitter_ang*rand)-jitter_ang;
    if a==ceil(length(xlocations)/2) && b==ceil(length(ylocations)/2)
      I=draw_bar(I,x,y,bar,angle(1),contrast(1),backgnd);
      xcent=x;
      ycent=y;
    else
      if rand<=surr_prob
        I=draw_bar(I,xval,yval,bar,angleval(2),contrast(2),backgnd);
      end
    end
  end
end
