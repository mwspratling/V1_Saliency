function plot_histogram_icons(I,trialstep,trialspacing,imwidth)
if nargin<3
  trialspacing=0.09;
end
if nargin<4
  imwidth=0.15;
end
ntrials=length(I);
hsize=25;
%hsize=50;
zmax=0;
for trial=1:trialstep:ntrials
  [a,b,z]=size(I{trial});
  zmax=max(zmax,z);
end
for trial=1:trialstep:ntrials
  [a,b,z]=size(I{trial});
  for t=zmax:-2:1
	offset=(zmax-t)*0.012;
	axes('Position',[0.045+trial*trialspacing-offset,0.39-4*offset,imwidth,imwidth]),
	imagesc(I{trial}(ceil(a/2)-hsize:ceil(a/2)+hsize,ceil(b/2)-hsize:ceil(b/2)+hsize,min(t,z))',[0,1]);
	axis('equal','tight')
	set(gca,'XTick',[],'YTick',[]);
  end
end
set(gcf,'PaperUnits','inches');
set(gcf,'PaperPosition',[0.95 0.418 6.36 3.33]);
