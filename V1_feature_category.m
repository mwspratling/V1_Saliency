function V1_feature_category
[imsizefac,crop,max_radius]=common_param_values;

%DEFINE TEST IMAGES
[barlen,barwid,spacing,gridsize]=standard_image_param_values(imsizefac,crop,2);
trials=1;

for trial=1:trials
  trial
  %CREATE IMAGES
  [I{1},ycoord{1},xcoord{1}]=image_popout(gridsize,barlen,barwid,spacing,[50,-10]);
  [I{2},ycoord{2},xcoord{2}]=image_popout(gridsize,barlen,barwid,spacing,[50,-50]);
  [I{3},ycoord{3},xcoord{3}]=image_popout_alt(gridsize,barlen,barwid,spacing,[-10,-50,50]);
  [I{4},ycoord{4},xcoord{4}]=image_popout_alt(gridsize,barlen,barwid,spacing,[-10,-70,30]);
  %[I{3},ycoord{3},xcoord{3}]=image_popout_alt(gridsize,barlen,barwid,spacing,[-10,50,-50]);
  %[I{4},ycoord{4},xcoord{4}]=image_popout_alt(gridsize,barlen,barwid,spacing,[-10,30,-70]);
  for test=1:length(I)
	cent_radius{test}=min(ceil(0.5*barlen+spacing),max_radius);
  end
  
  %PERFORM EXPERIMENT AND PLOT RESULTS
  saliency_index(:,trial)=...
	  perform_V1_saliency_experiment(I,cent_radius,ycoord,xcoord);
end
if trials>1
  saliency_index=mean(saliency_index')';
end
plot_saliency_histogram(I,saliency_index);

