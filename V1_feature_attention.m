function V1_feature_attention
[imsizefac,crop,max_radius]=common_param_values;

%DEFINE TEST IMAGES
[barlen,barwid,spacing,gridsize]=standard_image_param_values(imsizefac,crop,1);

[I{1},ycoord{1},xcoord{1}]=image_popout_overlayed(gridsize,barlen,barwid,spacing,[-45,45,0,0]);
[a,b]=size(I{1});
A{1}=initialise_feedback(I{1},[],[0,0,0,0]);

I{2}=I{1};ycoord{2}=ycoord{1};xcoord{2}=xcoord{1};
A{2}=initialise_feedback(I{2},[1:8:32,7:8:32],[1,a,1,b]);

I{3}=I{1};ycoord{3}=ycoord{1};xcoord{3}=xcoord{1};
A{3}=initialise_feedback(I{3},[1:8:32,3:8:32],[1,a,1,b]);

for test=1:length(I)
  cent_radius{test}=min(ceil(0.5*barlen+spacing),max_radius);
end

%PERFORM EXPERIMENT AND PLOT RESULTS
[saliency_index,poo,e]=...
	perform_V1_saliency_experiment(I,cent_radius,ycoord,xcoord,[],A);
%plot_error_histogram(I,e,1,saliency_index);
plot_saliency_histogram(I,saliency_index);

 
