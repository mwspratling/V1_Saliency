function V1_feature_spacing
[imsizefac,crop,max_radius]=common_param_values;
%DEFINE TEST IMAGES
barlen=(0.66*imsizefac)
barwid=(0.15*imsizefac)
spacings=round([0.8,1.1,1.4,1.8,2.4,3.5,5.7].*imsizefac);

%CREATE IMAGES
angle=45;
test=0;
for spacing=spacings
  test=test+1;
  gridsize=max(7,odd((70+2*crop)/spacing));

  [I{test},ycoord{test},xcoord{test}]=image_popout(gridsize,barlen,barwid,spacing,[angle,-angle]);
  %cent_radius{test}=ceil((0.5*barlen+spacing));
  cent_radius{test}=min(ceil((0.5*barlen+spacing)),max_radius);
end
%PERFORM EXPERIMENT AND PLOT RESULTS
saliency_index=perform_V1_saliency_experiment(I,cent_radius,ycoord,xcoord);

figure(1),clf
%subplot(3,1,[2:3]);
axes('Position',[0.1300    0.2100    0.7750    0.5154]);
plot(spacings./imsizefac,saliency_index,'b-s','LineWidth',4,'MarkerSize',12,'MarkerFaceColor','w');
set(gca,'FontSize',14,'Box','off');
ylabel('Saliency Index');
xlabel('Spacing (degrees)');
set(gca,'YTick',[-0.005:0.005:0.015],'YTickLabel',[-0.005:0.005:0.015]);
axis([0,6,-0.005,0.0175])

hsize=25;
plot_values=[1,4,6,7];
for test=1:length(plot_values)
  [a,b]=size(I{plot_values(test)});  
  axes('Position',[0.15+(test-1)*0.2,0.65,0.15,0.4]),
  imagesc(I{plot_values(test)}(ceil(a/2)-hsize:ceil(a/2)+hsize,ceil(b/2)-hsize:ceil(b/2)+hsize)',[0,1]);
  axis('equal','tight')
  set(gca,'XTick',[],'YTick',[]);
end
colormap('gray')
set(gcf,'PaperUnits','inches');
set(gcf,'PaperPosition',[0.95 0.418 6.36 3.33]);
