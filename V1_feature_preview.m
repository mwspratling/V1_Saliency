function V1_feature_preview
[imsizefac,crop,max_radius]=common_param_values;
onset_time=8;

%DEFINE TEST IMAGES
[barlen,barwid,spacing,gridsize]=standard_image_param_values(imsizefac,crop,1);

%CREATE IMAGE containing old and new elements
%[Iold,Inew,ycoord{1},xcoord{1}]=image_preview_dualdist(gridsize,barlen,barwid,spacing,[0,45,-45]);
[Iold,Inew,ycoord{1},xcoord{1}]=image_preview_conj(gridsize,barlen,barwid,spacing,[0,45,-45,90]);

I{1}=max(Iold,Inew); %search for target among all distractors
I{2}=Inew; %search for target among new distractors

%preview search
test=3;
for t=1:onset_time-1
  I{test}(:,:,t)=Iold;
end
for t=onset_time:10
  I{test}(:,:,t)=max(Iold,Inew); 
end

for test=1:length(I)
  ycoord{test}=ycoord{1};
  xcoord{test}=xcoord{1};
  cent_radius{test}=min(ceil(0.5*barlen+spacing),max_radius);
end

%PERFORM EXPERIMENT AND PLOT RESULTS
[saliency_index,poo,e]=...
	perform_V1_saliency_experiment(I,cent_radius,ycoord,xcoord);
plot_error_histogram(I,e,1);

