function V1_change_blindness
[imsizefac,crop,max_radius]=common_param_values;
onset_time1=6;
onset_time2=8;

%DEFINE TEST IMAGES
[barlen,barwid,spacing,gridsize]=standard_image_param_values(imsizefac,crop,1);

[image1,yval,xval]=image_popout_overlayed(gridsize,barlen,barwid,spacing,[-45,45,0,90]);
[image2,yval,xval]=image_popout_overlayed(gridsize,barlen,barwid,spacing,[45,45,0,90]);
flicker=0.5+(image1*0);

test=0;
%CREATE IMAGE SEQUENCES
%1) no flicker
test=test+1;
for t=1:onset_time1-1
  I{test}(:,:,t)=image1;
end
for t=onset_time1:10
  I{test}(:,:,t)=image2;
end
ycoord{test}=yval;
xcoord{test}=xval;

%2) with flicker
test=test+1;
for t=1:onset_time1-1
  I{test}(:,:,t)=image1;
end
for t=onset_time1:onset_time2-1
  I{test}(:,:,t)=flicker;
end
for t=onset_time2:10
  I{test}(:,:,t)=image2;
end
ycoord{test}=yval;
xcoord{test}=xval;

%3) unchanging 1st image
test=test+1;
for t=1:10
  I{test}(:,:,t)=image1;
end
ycoord{test}=yval;
xcoord{test}=xval;
for test=1:length(I)
  cent_radius{test}=min(ceil(0.5*barlen+spacing),max_radius);
end

%4) unchanging 2nd image
test=test+1;
for t=1:10
  I{test}(:,:,t)=image2;
end
ycoord{test}=yval;
xcoord{test}=xval;
for test=1:length(I)
  cent_radius{test}=min(ceil(0.5*barlen+spacing),max_radius);
end

%PERFORM EXPERIMENT AND PLOT RESULTS
saliency_index=perform_V1_saliency_experiment(I,cent_radius,ycoord,xcoord);
%pad saliency and images to allow more spacing in plot of results
saliency_index(7)=saliency_index(4);
I{7}=I{4};
saliency_index(5)=saliency_index(3);
I{5}=I{3};
saliency_index(3)=saliency_index(2);
I{3}=I{2};
saliency_index(2)=NaN;
saliency_index(4)=NaN;
saliency_index(6)=NaN;
plot_saliency_histogram(I,saliency_index,[],[],2);

