function V1_feature_symmetry
[imsizefac,crop,max_radius]=common_param_values;

%DEFINE TEST IMAGES
[barlen,barwid,spacing,gridsize]=standard_image_param_values(imsizefac,crop,2);
trials=16;
targets=[0,-10,80,90,50,-60,70,20,-30,40,90,60,30,0,-60,-30];
distractors=[30,40,50,60,20,30,40,50,60,70,30,20,70,60,20,70];

for trial=1:trials
  trial
  
  %CREATE IMAGES
  %vertically symmetrical distractors:
  [I{1},ycoord{1},xcoord{1}]=image_popout_alt(gridsize,barlen,barwid,spacing,[targets(trial),distractors(trial),-distractors(trial)]);
  %obliquely symmetrical distractors:
  [I{2},ycoord{2},xcoord{2}]=image_popout_alt(gridsize,barlen,barwid,spacing,[targets(trial),distractors(trial),-distractors(trial)]+50);
  for test=1:length(I)
    cent_radius{test}=min(ceil(0.5*barlen+spacing),max_radius);
  end
  
  %PERFORM EXPERIMENT AND PLOT RESULTS
  saliency_index(:,trial)=...
    perform_V1_saliency_experiment(I,cent_radius,ycoord,xcoord);
end
if trials>1
  saliency_index=mean(saliency_index')';
end
plot_saliency_histogram(I,saliency_index);

