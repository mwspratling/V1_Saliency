function V1_feature_asymmetric
[imsizefac,crop,max_radius]=common_param_values;

%DEFINE TEST IMAGES
[barlen,barwid,spacing,gridsize]=standard_image_param_values(imsizefac,crop,1);
angle=0;
[I{1},ycoord{1},xcoord{1}]=image_popout(gridsize,barlen,barwid,spacing,[0,20]);
[I{2},ycoord{2},xcoord{2}]=image_popout(gridsize,barlen,barwid,spacing,[20,0]);
[I{3},ycoord{3},xcoord{3}]=image_popout_vernier(gridsize,barlen,barwid,spacing,[0,0],[0.5,0.5],0.5,0,0,1,1);
[I{4},ycoord{4},xcoord{4}]=image_popout_vernier(gridsize,barlen,barwid,spacing,[0,0],[0.5,0.5],0.5,0,0,1,0);

[I{5},ycoord{5},xcoord{5}]=image_feature_sub(gridsize,barlen,barwid,spacing,angle);
[I{6},ycoord{6},xcoord{6}]=image_feature_add(gridsize,barlen,barwid,spacing,angle);

[I{7},ycoord{7},xcoord{7}]=image_popout_parallel_converged(gridsize,barlen,barwid,spacing,[-90,-90],[0.5,0.5],0.5,0,0,1,0);
[I{8},ycoord{8},xcoord{8}]=image_popout_parallel_converged(gridsize,barlen,barwid,spacing,[-90,-90],[0.5,0.5],0.5,0,0,1,1);

for test=1:length(I)
  cent_radius{test}=min(ceil(0.5*barlen+spacing),max_radius);
end

%PERFORM EXPERIMENT AND PLOT RESULTS
saliency_index=perform_V1_saliency_experiment(I,cent_radius,ycoord,xcoord);
plot_saliency_histogram(I,saliency_index,[]);
