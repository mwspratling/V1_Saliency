function [imsizefac,crop,max_radius,sigma]=common_param_values
sigma=[4];
crop=ceil(5*max(sigma));
max_radius=21;
imsizefac=10/1.6;
