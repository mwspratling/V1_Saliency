function [Iold,Inew,xcent,ycent]=image_preview_conj(gridsize,len,wid,spacing,angle,contrast,backgnd,jitter_pos,jitter_ang)
%draw a uniform texture containing two different orientations with a single
%element at a different orientation and/or contrast
if nargin<6, contrast=0.5*ones(size(angle)); end
if nargin<7, backgnd=0.5; end
if nargin<8, jitter_pos=0; end
if nargin<9, jitter_ang=0; end

bar=define_bar(len,wid);
[Inew,gridsize,xlocations,ylocations]=define_blank_image(gridsize,len,spacing,backgnd);
Iold=Inew;

a=0;
for x=xlocations
  a=a+1;
  b=0;
  for y=ylocations
	b=b+1;
	xval=fix(x+(2*jitter_pos*rand)-jitter_pos);
	yval=fix(y+(2*jitter_pos*rand)-jitter_pos);
	angleval=angle+(2*jitter_ang*rand)-jitter_ang;
	if a==ceil(length(xlocations)/2) & b==ceil(length(ylocations)/2)
	  Inew=draw_bar(Inew,x,y,bar,angle(1),contrast(1),backgnd);
	  Inew=draw_bar(Inew,x,y,bar,angle(2),contrast(1),backgnd);
	  xcent=x;
	  ycent=y;
	else
	  if rand<0.5
		Iold=draw_bar(Iold,xval,yval,bar,angleval(1),contrast(2),backgnd);
		Iold=draw_bar(Iold,xval,yval,bar,angleval(3),contrast(2),backgnd);
	  else 	  
		Inew=draw_bar(Inew,xval,yval,bar,angleval(4),contrast(2),backgnd);	  
		Inew=draw_bar(Inew,xval,yval,bar,angleval(2),contrast(2),backgnd);	  
	  end
	end
  end
end
