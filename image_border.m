function [I,xcent,ycent]=image_border(gridsize,len,wid,spacing,angle,contrast,backgnd,jitter_pos,jitter_ang)
%draw an image consisting of two halves each made up of a regular texture of
%edge elements at the defined angle
if length(angle)<2, angle(2)=-angle; end
if nargin<6, contrast=0.5*ones(size(angle)); end
if nargin<7, backgnd=0.5; end
if nargin<8, jitter_pos=0; end
if nargin<9, jitter_ang=0; end

bar=define_bar(len,wid);
[I,gridsize,xlocations,ylocations]=define_blank_image(gridsize,len,spacing,backgnd);

a=0;
for x=xlocations
  a=a+1;
  b=0;
  for y=ylocations
	b=b+1;
	if x>min(xlocations) && x<max(xlocations) && y>min(ylocations) && y<max(ylocations)
	  xval=x+floor((2*jitter_pos*rand)-jitter_pos);
	  yval=y+floor((2*jitter_pos*rand)-jitter_pos);
	else
	  xval=x;
	  yval=y;
	end
	angleval=angle+(2*jitter_ang*rand)-jitter_ang;
	if a<=ceil(length(xlocations)/2)
	  I=draw_bar(I,xval,yval,bar,angleval(1),contrast(1),backgnd);
	else
	  I=draw_bar(I,xval,yval,bar,angleval(2),contrast(2),backgnd);
	end
	if a==ceil(length(xlocations)/2) && b==ceil(length(ylocations)/2)
	  xcent=x;
	  ycent=y;
	end
	if a==ceil(length(xlocations)/2)+1 && b==ceil(length(ylocations)/2);
	  xcent=mean([xcent,x]);
	end
  end
end
