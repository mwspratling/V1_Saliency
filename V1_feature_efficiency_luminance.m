function V1_feature_efficiency_luminance
[imsizefac,crop,max_radius]=common_param_values;
%DEFINE TEST IMAGES
[barlen,barwid,spacing,gridsize]=standard_image_param_values(imsizefac,crop,1);
barwid=barlen;
angle=0;
backgrnd=0; polarity=1; %for black background with light targets/distractors
%backgrnd=1; polarity=-1; %for white background with dark targets/distractors
trials=20;
for trial=1:trials
  trial
  %CREATE IMAGES
  [I{1},ycoord{1},xcoord{1}]=image_popout(gridsize,barlen,barwid,spacing,[angle,angle],polarity.*[1,0.25],backgrnd);
  [I{2},ycoord{2},xcoord{2}]=image_popout(gridsize,barlen,barwid,spacing,[angle,angle],polarity.*[1,0.5],backgrnd);
  [I{3},ycoord{3},xcoord{3}]=image_popout(gridsize,barlen,barwid,spacing,[angle,angle],polarity.*[1,0.75],backgrnd);
  
  [I{4},ycoord{4},xcoord{4}]=image_popout(gridsize,barlen,barwid,spacing,[angle,angle],polarity.*[0.25,1],backgrnd);
  [I{5},ycoord{5},xcoord{5}]=image_popout(gridsize,barlen,barwid,spacing,[angle,angle],polarity.*[0.5,1],backgrnd);
  [I{6},ycoord{6},xcoord{6}]=image_popout(gridsize,barlen,barwid,spacing,[angle,angle],polarity.*[0.75,1],backgrnd);
  
  [I{7},ycoord{7},xcoord{7}]=image_popout_dualdist(gridsize,barlen,barwid,spacing,[angle,angle,angle],polarity.*[1,0.75,0.5],backgrnd);
  [I{8},ycoord{8},xcoord{8}]=image_popout_dualdist(gridsize,barlen,barwid,spacing,[angle,angle,angle],polarity.*[0.5,1,0.75],backgrnd);
  [I{9},ycoord{9},xcoord{9}]=image_popout_dualdist(gridsize,barlen,barwid,spacing,[angle,angle,angle],polarity.*[0.75,1,0.5],backgrnd);
  for test=1:length(I)
	cent_radius{test}=min(ceil((0.5*barlen+spacing)),max_radius);
  end

  %PERFORM EXPERIMENT AND PLOT RESULTS
  saliency_index(:,trial)=perform_V1_saliency_experiment(I,cent_radius,ycoord,xcoord);
end
if trials>1
  saliency_index=mean(saliency_index')';
end
plot_saliency_histogram(I,saliency_index,[]);
