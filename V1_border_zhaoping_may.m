function V1_border_zhaoping_may
[imsizefac,crop,max_radius]=common_param_values;

%DEFINE TEST IMAGES
[barlen,barwid,spacing,gridsize]=standard_image_param_values(imsizefac,crop,1);
gridsize=gridsize-1;
[I{1},ycoord{1}]=image_border(gridsize,barlen,barwid,spacing,[-45,45]);
[I{5},ycoord{5}]=image_border(gridsize,barlen,barwid,spacing,[90,0]);
[I{4},ycoord{4}]=image_border_overlayed(gridsize,barlen,barwid,spacing,[-45,45,0,90]);
[I{7},ycoord{7}]=image_border_overlayed(gridsize,barlen,barwid,spacing,[90,0,-45,45]);
[I{2},ycoord{2}]=image_border_overlayed(gridsize,barlen,barwid,spacing,[-45,45,90,90]);
[I{6},ycoord{6}]=image_border_overlayed(gridsize,barlen,barwid,spacing,[90,0,-45,-45]);
[I{3},ycoord{3}]=image_border_overlayed(gridsize,barlen,barwid,spacing,[-45,45,0,0]);
%[I{8},ycoord{8}]=image_border(gridsize,barlen,barwid,spacing,[-10,10]);
%[I{9},ycoord{9}]=image_border_alt(gridsize,barlen,barwid,spacing,[-67.5,-22.5,67.5,22.5]);
%[I{10},ycoord{10}]=image_border_overlayed(gridsize,barlen,barwid,spacing,[-45,45,80,-80]);
%[I{11},ycoord{11}]=image_border_overlayed(gridsize,barlen,barwid,spacing,[-10,10,80,-80]);
for test=1:length(I)
  cent_radius{test}=min(ceil(spacing),max_radius);
end
%PERFORM EXPERIMENT AND PLOT RESULTS
saliency_index=perform_V1_saliency_experiment(I,cent_radius,ycoord);

%Zhoaping and May results (approx), normalised RTs:
RT=[1 1.15 1.22 3.13 0.94 0.97 1.02];%,     1.3   1.1   1.6   2.5];
plot_saliency_histogram(I,saliency_index,RT',0.1);
%plot_saliency_histogram(I,saliency_index);

