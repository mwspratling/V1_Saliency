function plot_error_histogram(I,e,trialstep,saliency_index)
figure(1),clf
ntrials=length(I);
if nargin<3
  trialstep=1; 
end
trialspacing=0.18;
imwidth=0.15;
height=0.55;
if ntrials>=5
  trialspacing=0.12;
  imwidth=0.1;
  height=0.44;
end
power=1;
emax=max(1.00001,max(max(max(cat(3,e{:}))))).^power
%emax=2.9433
%emax=2.6
hsize=25;
for trial=1:ntrials
  axes('Position',[0.045+trial*trialspacing,height,imwidth,0.35]),
  [a,b]=size(e{trial});
  imagesc(e{trial}(ceil(a/2)-hsize:ceil(a/2)+hsize,ceil(b/2)-hsize:ceil(b/2)+hsize)'.^power,[1,emax]);
  colormap('gray')
  axis('equal','tight')
  set(gca,'XTick',[],'YTick',[]);
  %colorbar('NorthOutside');
  if nargin>=4
	xlabel(['SI=',num2str(saliency_index(trial),2)],'FontSize',7,'VerticalAlignment','middle');
  end
end
plot_histogram_icons(I,trialstep,trialspacing,imwidth)
