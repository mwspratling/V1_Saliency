function [I,xcent,ycent]=image_contour(gridsize,len,wid,spacing,angle,clen,contrast,backgnd,jitter_pos,jitter_ang)
if nargin<5, angle=0; end
if nargin<6, clen=gridsize; end
if nargin<7, contrast=[0.5,0.5]; end
if nargin<8, backgnd=0.5; end
if nargin<9, jitter_pos=0; end
if nargin<10, jitter_ang=0; end

bar=define_bar(len,wid);
[I,gridsize,xlocations,ylocations]=define_blank_image(gridsize,len,spacing,backgnd);

a=0;
for x=xlocations
  a=a+1;
  b=0;
  for y=ylocations
	b=b+1;
	if x>min(xlocations) & x<max(xlocations) & y>min(ylocations) & y<max(ylocations)
	  xval=x+floor((2*jitter_pos*rand)-jitter_pos);
	  yval=y+floor((2*jitter_pos*rand)-jitter_pos);
	else
	  xval=x;
	  yval=y;
	end
	angleval=angle+(2*jitter_ang*rand)-jitter_ang;
	if a==ceil(length(xlocations)/2) & b>=ceil(length(ylocations)/2)-clen & b<=ceil(length(ylocations)/2)+clen
	  I=draw_bar(I,x,y,bar,angleval,contrast(1),backgnd);
	else
	  I=draw_bar(I,xval,yval,bar,rand*180,contrast(2),backgnd);
	end
	if a==ceil(length(xlocations)/2) & b==ceil(length(ylocations)/2)
	  xcent=x;
	  ycent=y;
	end
  end
end
