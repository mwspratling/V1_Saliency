function [I,xcent,ycent]=image_feature_add(gridsize,len,wid,spacing,angle,contrast,backgnd)
%create a search array with odd target defined by additional element
if nargin<6, contrast=[0.5,0.5]; end
if nargin<7, backgnd=0.5; end

bar=define_bar(len,wid);
[I,gridsize,xlocations,ylocations]=define_blank_image(gridsize,len,spacing,backgnd);

a=0;
for x=xlocations
  a=a+1;
  b=0;
  for y=ylocations
	b=b+1;
	I=draw_bar(I,x,y,bar,angle,contrast(2),backgnd);
	if a==ceil(length(xlocations)/2) && b==ceil(length(ylocations)/2)
  	I=draw_bar(I,x,y,bar,angle,contrast(1),backgnd);
	  I=draw_bar(I,x,y,bar,angle-90,contrast(1),backgnd);
	  xcent=x;
	  ycent=y;
	end
  end
end

