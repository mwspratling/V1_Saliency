function [I,xcent,ycent]=image_popout_parallel_converged(gridsize,len,wid,spacing,angle,contrast,backgnd,jitter_pos,jitter_ang,surr_prob,cent_conv)
%draw a uniform texture with a single element at a different orientation
%and/or contrast
if nargin<6, contrast=0.5*ones(size(angle)); end
if nargin<7, backgnd=0.5; end
if nargin<8, jitter_pos=0; end
if nargin<9, jitter_ang=0; end
if nargin<10, surr_prob=1; end
if nargin<11, cent_conv=1; end
barang=10;
baroffset=5;

[I,gridsize,xlocations,ylocations]=define_blank_image(gridsize,len+baroffset,spacing,backgnd);
bar=define_bar(len,wid);
blank=zeros(size(bar)+baroffset-1);
[a,b]=size(blank);

parallel=draw_bar(blank,ceil(a/2)-floor(baroffset/2),ceil(b/2),bar,0,1,0);
parallel=draw_bar(parallel,ceil(a/2)+floor(baroffset/2),ceil(b/2),bar,0,1,0);
converged=draw_bar(blank,ceil(a/2)-floor(baroffset/2),ceil(b/2),bar,barang,1,0);
converged=draw_bar(converged,ceil(a/2)+floor(baroffset/2),ceil(b/2),bar,-barang,1,0);

a=0;
for x=xlocations
  a=a+1;
  b=0;
  for y=ylocations
	b=b+1;
	xval=fix(x+(2*jitter_pos*rand)-jitter_pos);
	yval=fix(y+(2*jitter_pos*rand)-jitter_pos);
	angleval=angle+(2*jitter_ang*rand)-jitter_ang;
	if a==ceil(length(xlocations)/2) & b==ceil(length(ylocations)/2)
	  if cent_conv
		  I=draw_bar(I,xval,yval,converged,angleval(1),contrast(1),backgnd);
		else
		  I=draw_bar(I,xval,yval,parallel,angleval(1),contrast(1),backgnd);
	  end
	  xcent=x;
	  ycent=y;
	else
	  if rand<=surr_prob
		if cent_conv		
		  I=draw_bar(I,xval,yval,parallel,angleval(2),contrast(2),backgnd);
		else
		  I=draw_bar(I,xval,yval,converged,angleval(2),contrast(2),backgnd);
		end
	  end
	end
  end
end
